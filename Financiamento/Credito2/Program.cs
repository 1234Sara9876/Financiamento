﻿using Financiamento.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Credito2
{
    class Program
    {
        static void Main(string[] args)
        {
            string resp = string.Empty;

            Console.Write("Pretende uma simulação de crédito à habitação, S ou N?");
            resp = Console.ReadLine().ToUpper();
            while (resp != "S" && resp != "N")
            {
                Console.WriteLine("Responda S ou N; tente de novo, por favor ");
            }

            if (resp == "S")
            {
                Console.Clear();
                CrediCasa();
            }
            else
            {
                Console.Write("\nPretende uma simulação de crédito automóvel, S ou N?");
                resp = Console.ReadLine().ToUpper();
                while (resp != "S" && resp != "N")
                {
                    Console.WriteLine("Responda S ou N; tente de novo, por favor ");
                }
                if (resp == "S")
                {
                    Console.Clear();
                    CrediAuto();
                }
                else
                {
                    Console.Clear();
                    Crediversos();
                }
            }
        }

        private static void Crediversos()
        {
            decimal montantetotal = 0.0m;
            int prazoMeses = 0;

            Console.WriteLine("\n    C R É D I T O   A O   C O N S U M O");

            Console.Write("\nIntroduza a quantia de empréstimo que pretende:");
            while (!decimal.TryParse(Console.ReadLine(), out montantetotal))
                Console.WriteLine("Número não válido; tente de novo, por favor: ");

            if (montantetotal>5000)
            {
                Console.Write("A quantia de empréstimo não pode ultrapassar os 5000 euros");
                Console.ReadKey();
                return;
            }
               
            Console.Write("Introduza em quantos meses pretende pagar o montante :");
            while (!int.TryParse(Console.ReadLine(), out prazoMeses))
                Console.WriteLine("Número não válido; tente de novo, por favor: ");

            CreDiversos crediVersos = new CreDiversos(montantetotal, prazoMeses, 0.093m, 0.0001m, 0.0005m);

            Console.WriteLine(crediVersos);
            Console.ReadKey();
        }

        private static void CrediCasa()
        {
            int prazoAnos = 0;
            int prazoMeses;
            decimal rendimento = 0.0m;
            Fiador fiador = new Fiador();

            Console.WriteLine("\n    C R É D I T O   À   H A B I T A C A O");
            Console.Write("\nIntroduza o seu rendimento mensal bruto em euros :");
            while (!decimal.TryParse(Console.ReadLine(), out rendimento))
                Console.WriteLine("Número não válido; tente de novo: ");
           
            Console.Write("Introduza em quantos anos pretende pagar a casa :");
            while (!int.TryParse(Console.ReadLine(), out prazoAnos))
                Console.WriteLine("Número não válido; tente de novo: ");

            prazoMeses = 12 * prazoAnos;

            fiador.Nome = string.Empty;
            fiador.Morada = string.Empty;
            fiador.Telemovel = 999999999;
            fiador.Nif=999999999;
            fiador.RendimentoAnual = 0.0m;
            
            CrediHabitacao crediHabita = new CrediHabitacao(250000.00m, prazoMeses, 0.0m, 0.015m, 0.0027m, fiador);

            if (rendimento < 2*crediHabita.Prestacoes())
            {
                string resp = string.Empty;
                Console.Write("A proporção entre  o seu rendimento mensal e o crédito pretendido exige um fiador. \nPretende introduzir os dados pessoais do seu fiador?");
                resp = Console.ReadLine().ToUpper();
                while (resp != "S" && resp != "N")
                {
                    Console.WriteLine("Responda S ou N; tente de novo, por favor ");
                }
                if (resp == "S")
                {
                    InscreveFiador(fiador, crediHabita);
                }
                if (crediHabita.Fiador.Nome == string.Empty|| crediHabita.Fiador.RendimentoAnual < 2 * 12 * crediHabita.Prestacoes())
                    return;
            }
            Console.WriteLine(crediHabita);
            Console.ReadKey();
        }

        private static void InscreveFiador(Fiador fiador, CrediHabitacao crediHabita)
        {
            int tlf = 0;
            decimal rent = 0;

            Console.WriteLine("Introduza o nome completo");
            fiador.Nome = Console.ReadLine();

            Console.WriteLine("Introduza o local de residência");
            fiador.Morada = Console.ReadLine();

            Console.WriteLine("Introduza o número de telemóvel");
            while (!int.TryParse(Console.ReadLine(), out tlf))
                Console.WriteLine("Número não válido; tente de novo: ");
            fiador.Telemovel = tlf;

            Console.WriteLine("Introduza o nº de contribuinte");
            while (!int.TryParse(Console.ReadLine(), out tlf))
                Console.WriteLine("Número não válido; tente de novo: ");
            fiador.Nif = tlf;

            Console.WriteLine("Introduza o rendimento anual bruto do seu fiador");
            while (!decimal.TryParse(Console.ReadLine(), out rent))
                Console.WriteLine("Número não válido; tente de novo: ");
            fiador.RendimentoAnual = rent;
            if (rent/12<2*crediHabita.Prestacoes())
            {
                Console.Write("A proporção entre  o rendimento do seu fiador e o crédito pretendido não é suficiente para esta habitação.");
                Console.ReadKey();
                return;
            }
            else
            {
                crediHabita.Fiador = fiador;
            }
        }

        private static void CrediAuto()
        {
            int duracaoMeses = 0;
            int prazoMeses = 0;

            Console.WriteLine("\n    C R É D I T O   A U T O M O V E L");
            Console.Write("\nIntroduza em quantos meses pretende pagar o automóvel :");
            while (!int.TryParse(Console.ReadLine(), out duracaoMeses))
                Console.WriteLine("Número não válido; tente de novo: ");

            if (duracaoMeses > 60)
                prazoMeses = 60;
            else
                prazoMeses = duracaoMeses;

            CrediCarro crediCarro = new CrediCarro(18000.00m, prazoMeses, 0.02m, 0.0001m, 0.01m);

            Console.WriteLine(crediCarro);
            Console.ReadKey();
        }
    }
}
