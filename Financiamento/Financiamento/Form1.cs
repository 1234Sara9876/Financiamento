﻿namespace Financiamento
{
    using Financiamento.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private List<string> creditos;
        private CrediHabitacao crediHabita;
        private OFiador fFiador;
        private Fiador fiador;
        private int prazoMeses = 0;
        private decimal rendimento = 0.0m;
        private CrediCarro crediCarro;
        private CreDiversos creDiversos;
        private decimal montante=0.0m;

        public Form1()
        {
            InitializeComponent();
            creditos = new List<string>() { "Crédito à Habitação", "Crédito Automóvel", "Crédito ao Consumo" };
            cbFinanciamento.DataSource = creditos;
            fiador = new Fiador()
            { Nome = string.Empty, Morada = string.Empty, Telemovel = 999999999, Nif = 999999999, RendimentoAnual = 0.0m };
            crediHabita = new CrediHabitacao(250000.00m, 12, 0.0m, 0.015m, 0.0027m, fiador);
            crediCarro = new CrediCarro(18000.00m, 60, 0.02m, 0.0001m, 0.01m);
            creDiversos = new CreDiversos(5000.00m, 3, 0.093m, 0.01m, 0.0005m);
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            if (cbFinanciamento.SelectedIndex == -1)
            {
                MessageBox.Show("Seleccione o tipo de financiamento a simular");
                return;
            }
            if (cbFinanciamento.SelectedIndex == 0)
            {
                gbCrediHab.Enabled = true;
                gbCrediAuto.Enabled = false;
                gbCrediversos.Enabled = false;
                lblMontante2.Text = "250 000 euros";
               
            }
            if (cbFinanciamento.SelectedIndex == 1)
            {
                gbCrediAuto.Enabled = true;
                gbCrediHab.Enabled = false;
                gbCrediversos.Enabled = false;
                lblMontante1.Text= "18 000 euros";
            }
            if (cbFinanciamento.SelectedIndex == 2)
            {
                gbCrediversos.Enabled = true;
                gbCrediAuto.Enabled = false;
                gbCrediHab.Enabled = false;
            }
        }

        //métodos do crédito automóvel
        private void udPrazo1_ValueChanged(object sender, EventArgs e)
        {
            prazoMeses = Convert.ToInt16(udPrazo1.Value);
            if (prazoMeses == 0)
            {
                MessageBox.Show("Introduza o prazo de pagamento , por favor");
                return;
            }
            CrediAuto();
        }

        private void CrediAuto()
        {
            if (prazoMeses > 60)
                crediCarro.PrazoPagamento = 60;
            else
                crediCarro.PrazoPagamento = prazoMeses;
            button2.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SimulaCrediAuto();
        }

        private void SimulaCrediAuto()
        {
            udTaxaJuro1.Enabled = true;
            udTaxaJuro1.Value = crediCarro.JuroAnual;
            udComissao1.Enabled = true;
            udComissao1.Value = crediCarro.ComissaoAbertura;
            udValorResidual.Enabled = true;
            udValorResidual.Value = crediCarro.ValorResidual;
            udPrestacoes1.Enabled = true;
            udPrestacoes1.Value = crediCarro.Prestacoes();
        }

        //métodos do crédito à habitação
        private void udPrazo2_ValueChanged(object sender, EventArgs e)
        {
            prazoMeses = 12 * Convert.ToInt16(udPrazo2.Value);
            if (prazoMeses == 0)
            {
                MessageBox.Show("Introduza o prazo de pagamento , por favor");
                return;
            }
        }

        private void udRendimento_ValueChanged(object sender, EventArgs e)
        {
            rendimento = udRendimento.Value;
            if (rendimento == 0)
            {
                MessageBox.Show("Introduza o seu rendimento mensal , por favor");
                return;
            }
            CrediHabita();
        }

        private void CrediHabita()
        {
            crediHabita.PrazoPagamento = prazoMeses;
            if (rendimento < 2 * crediHabita.Prestacoes())
            {
                MessageBox.Show("A proporção entre  o seu rendimento mensal e o crédito pretendido exige um fiador. Introduza por favor os dados pessoais do fiador do seu crédito");
                fFiador = new OFiador(crediHabita);
                fFiador.Show();
            }
            button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SimulaCreditoHab();
        }

        private void SimulaCreditoHab()
        {
            udTaxaJuro2.Enabled = true;
            udTaxaJuro2.Value = crediHabita.JuroAnual;
            udSpread.Enabled = true;
            udSpread.Value = crediHabita.Spread;
            udEuribor.Enabled = true;
            udEuribor.Value = crediHabita.Euribor;
            udPrestacoes2.Enabled = true;
            udPrestacoes2.Value = crediHabita.Prestacoes();
        }

        //métodos do crédito ao consumo
        private void udMontante3_ValueChanged(object sender, EventArgs e)
        {
            montante = udMontante3.Value;
            if (montante == 0)
            {
                MessageBox.Show("Introduza o valor do empréstimo pretendido , por favor");
                return;
            }
        }

        private void udPrazo3_ValueChanged(object sender, EventArgs e)
        {
            prazoMeses = Convert.ToInt16(udPrazo3.Value);
            if (prazoMeses == 0)
            {
                MessageBox.Show("Introduza o prazo de pagamento , por favor");
                return;
            }
            CrediVersos();
        }

        private void CrediVersos()
        {
            creDiversos.PrazoPagamento = prazoMeses;
            if (montante >=5000)
            {
                MessageBox.Show("A quantia de empréstimo não pode ultrapassar os 5000 euros");
                return;
            }
            button3.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            SimulaCrediversos();
        }

        private void SimulaCrediversos()
        {
            udTaxaJuro3.Enabled = true;
            udTaxaJuro3.Value = creDiversos.JuroAnual;
            udComissao2.Enabled = true;
            udComissao2.Value = creDiversos.ComissaoAbertura;
            udTaxaAmortizacao.Enabled = true;
            udTaxaAmortizacao.Value = creDiversos.TxAmortizacao;
            udPrestacoes3.Enabled = true;
            udPrestacoes3.Value = creDiversos.Prestacoes();
        }
    }
}

