﻿using Financiamento.Modelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Financiamento
{
    public partial class OFiador : Form
    {
        //private Fiador fiador = new Fiador();
        private CrediHabitacao credit;

        public OFiador(CrediHabitacao crediHabita)
        {
            InitializeComponent();
            credit = crediHabita;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbNome.Text))
            {
                MessageBox.Show("Tem que inserir o nome do seu fiador");
                return;
            }
            if (udRend.Value <= 0)
            {
                MessageBox.Show("Introduza o rendimento anual do seu fiador; esta informação é imprescindível para simular o crédito");
                return;
            }
            credit.Fiador.Nome = tbNome.Text;
            credit.Fiador.Morada = tbMorada.Text;
            credit.Fiador.Telemovel = Convert.ToInt32(udTelemovel.Value);
            credit.Fiador.Nif= Convert.ToInt32(udNif.Value);
            credit.Fiador.RendimentoAnual = udRend.Value;

            if (credit.Fiador.RendimentoAnual < 2 * 12 * credit.Prestacoes())
            {
                MessageBox.Show("O rendimento do seu fiador não é suficiente para esta habitação");
                return;
            }
             Close();
        }
    }
}
