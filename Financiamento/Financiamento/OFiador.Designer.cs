﻿namespace Financiamento
{
    partial class OFiador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNome = new System.Windows.Forms.TextBox();
            this.tbMorada = new System.Windows.Forms.TextBox();
            this.udTelemovel = new System.Windows.Forms.NumericUpDown();
            this.udNif = new System.Windows.Forms.NumericUpDown();
            this.udRend = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.udTelemovel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRend)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(48, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Completo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(48, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Morada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(48, 175);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Telemóvel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(182, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "NIF (Nº de Contribuinte)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(48, 340);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(184, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Rendimento Anual Bruto";
            // 
            // tbNome
            // 
            this.tbNome.Location = new System.Drawing.Point(286, 32);
            this.tbNome.Name = "tbNome";
            this.tbNome.Size = new System.Drawing.Size(379, 22);
            this.tbNome.TabIndex = 5;
            // 
            // tbMorada
            // 
            this.tbMorada.Location = new System.Drawing.Point(286, 96);
            this.tbMorada.Name = "tbMorada";
            this.tbMorada.Size = new System.Drawing.Size(379, 22);
            this.tbMorada.TabIndex = 6;
            // 
            // udTelemovel
            // 
            this.udTelemovel.Location = new System.Drawing.Point(286, 173);
            this.udTelemovel.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.udTelemovel.Name = "udTelemovel";
            this.udTelemovel.Size = new System.Drawing.Size(219, 22);
            this.udTelemovel.TabIndex = 7;
            // 
            // udNif
            // 
            this.udNif.Location = new System.Drawing.Point(286, 247);
            this.udNif.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.udNif.Name = "udNif";
            this.udNif.Size = new System.Drawing.Size(219, 22);
            this.udNif.TabIndex = 8;
            // 
            // udRend
            // 
            this.udRend.Location = new System.Drawing.Point(286, 338);
            this.udRend.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.udRend.Name = "udRend";
            this.udRend.Size = new System.Drawing.Size(219, 22);
            this.udRend.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(578, 267);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 67);
            this.button1.TabIndex = 10;
            this.button1.Text = "Gravar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // OFiador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(743, 403);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.udRend);
            this.Controls.Add(this.udNif);
            this.Controls.Add(this.udTelemovel);
            this.Controls.Add(this.tbMorada);
            this.Controls.Add(this.tbNome);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "OFiador";
            this.Text = "Dados Pessoais do Fiador";
            ((System.ComponentModel.ISupportInitialize)(this.udTelemovel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udNif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRend)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbNome;
        private System.Windows.Forms.TextBox tbMorada;
        private System.Windows.Forms.NumericUpDown udTelemovel;
        private System.Windows.Forms.NumericUpDown udNif;
        private System.Windows.Forms.NumericUpDown udRend;
        private System.Windows.Forms.Button button1;
    }
}