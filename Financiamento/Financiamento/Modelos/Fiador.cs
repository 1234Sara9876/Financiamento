﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class Fiador
    {
        public string Nome { get; set; }
        public string Morada { get; set; }
        public int Telemovel { get; set; }
        public int Nif { get; set; }
        public decimal RendimentoAnual { get; set; }
    }
}
