﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class CrediHabitacao: Credito
    {
        public Fiador Fiador { get; set; }
        public decimal Euribor { get; }
        public decimal Spread { get; }

        public CrediHabitacao(decimal montante, int prazoPagamento, decimal juroAnual, decimal spread, decimal euribor, Fiador fiador) : base(montante, prazoPagamento, juroAnual)
        {
            Euribor = euribor;
            Spread = spread;
            Fiador = fiador;
            JuroAnual = euribor + spread;
        }
        //método q traz a atribuição de salário todos os meses 
        public override decimal Prestacoes()
        {
            return (base.Montante * (1 + base.JuroAnual))/ base.PrazoPagamento - 1;
        }

        public override string ToString()
        {
            return $"\nCrédito Habitação -> \n{base.ToString()} composto por:\nSpread: {Spread*100:C2} %, \nEuribor: {Euribor*100:C2} %, \nPrestação mensal: {Prestacoes():C2} euros";
        }
    }
}
