﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class Crediversos: Credito
    {
        private decimal comissaoAbertura;
        private decimal txAmortizacao;
        private decimal valorAmortizado;
        private decimal mesesDecorridos;
       
        public Crediversos(decimal montante, int prazoPagamento, decimal juroAnual, decimal comissaoAbertura, decimal txAmortizacao,decimal valorAmortizado,decimal mesesDecorridos) :base(montante, prazoPagamento, juroAnual)
        {
            this.comissaoAbertura = comissaoAbertura;
            this.txAmortizacao = txAmortizacao;
            this.mesesDecorridos = mesesDecorridos;
        }
        public decimal valorTxAmortizacao(decimal txAmortizacao, decimal valorAmortizado)
        {
            return txAmortizacao * valorAmortizado;
        }
        //método q determina a prestação mensal 
        public override decimal Prestacoes()
        {
            if (valorAmortizado>=0)
            {
                return (base.Montante - valorAmortizado / base.PrazoPagamento-mesesDecorridos) * (1 + base.JuroAnual);
            }
            else
                return (base.Montante  / base.PrazoPagamento ) * (1 + base.JuroAnual);
        }

        public override string ToString()
        {
            return $"\nCrédito Automóvel -> \n{base.ToString()} , \nComissão de abertura: {comissaoAbertura}, \nValor residual (a pagar no último mês): {valorResidual}, \nPrestação mensal: {Prestacoes():C2}";
        }
    }
}
