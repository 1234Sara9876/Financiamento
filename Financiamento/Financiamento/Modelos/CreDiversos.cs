﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class CreDiversos: Credito
    {
        public decimal ComissaoAbertura { get; }
        public decimal TxAmortizacao { get; }
       
        public CreDiversos(decimal montante, int prazoPagamento, decimal juroAnual, decimal comissaoAbertura, decimal txAmortizacao ) :base(montante, prazoPagamento, juroAnual)
        {
            ComissaoAbertura = comissaoAbertura * (base.Montante * (1 + base.JuroAnual));
            TxAmortizacao = txAmortizacao;
        }

        //método que determina a taxa a pagar e actualiza os dados quando se amortiza; a utilizar qd se tiver uma base de dados
        public decimal valorTxAmortizacao (decimal valorAmortizado, int mesesDecorridos)
        {
            base.Montante -= valorAmortizado;
            base.PrazoPagamento -= mesesDecorridos;
            return TxAmortizacao * valorAmortizado;
        }

        //método q determina a prestação mensal 
        public override decimal Prestacoes()
        {
            return (base.Montante  / base.PrazoPagamento ) * (1 + base.JuroAnual);
        }

        public override string ToString()
        {
            return $"\nCrédito ao Consumo -> \n{base.ToString()} , \nComissão de abertura: {ComissaoAbertura:C2} euros, \nTaxa de Amortizacao Antecipada: {TxAmortizacao*100} %, \nPrestação mensal: {Prestacoes():C2} euros";
        }
    }
}
