﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class CrediHab: Credito
    {
        private decimal spread;
        private Fiador fiadorCopiado;
        private decimal euribor;

        public CrediHab(decimal montante, int prazoPagamento, decimal juroAnual, decimal spread, decimal euribor, Fiador fiador) :base(montante, prazoPagamento, juroAnual)
        {
            this.spread = spread;
            fiador = fiadorCopiado;
            this.euribor = euribor;
            base.JuroAnual = spread + euribor;
        }
        //método q determina a prestação mensal 
        public override decimal Prestacoes()
        {
            return (base.Montante * (1 + base.JuroAnual) ) / base.PrazoPagamento;
        }

        public override string ToString()
        {
            return $"\nCrédito à Habitação -> \n{base.ToString()} , \nSpread: {spread}, \nTaxa Euribor a 6 meses: {euribor}, \nFiador:{fiadorCopiado.Nome}, \nPrestação mensal: {Prestacoes()}";
        }
    }
}
