﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public abstract class Credito
    {
        public decimal Montante { get; set; }
        public int PrazoPagamento { get; set; }
        public decimal JuroAnual{ get; set; }

        public Credito(decimal montante, int prazoPagamento, decimal juroAnual)
        {
            Montante = montante;
            PrazoPagamento =prazoPagamento;
            JuroAnual = juroAnual;
        }
        public override string ToString()
        {
            {
                return $"Montante do empréstimo: {Montante} euros, \nPrazo de pagamento : {PrazoPagamento} meses, \nTaxa de juro anual bruta: {JuroAnual*100:C2} %";
            }
        }
        public abstract decimal Prestacoes();
    }
}
