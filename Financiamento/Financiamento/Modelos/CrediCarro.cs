﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento.Modelos
{
    public class CrediCarro: Credito
    {
        public decimal ComissaoAbertura { get; }
        public decimal ValorResidual { get; }

        public CrediCarro(decimal montante, int prazoPagamento, decimal juroAnual, decimal comissaoAbertura, decimal valorResidual) :base(montante, prazoPagamento, juroAnual)
        {
            ComissaoAbertura = comissaoAbertura * (base.Montante * (1 + base.JuroAnual));
            ValorResidual = valorResidual * (base.Montante * (1 + base.JuroAnual)) ;
        }
        //método q traz a atribuição de salário todos os meses 
        public override decimal Prestacoes()
        {
            return ((base.Montante * (1 + base.JuroAnual)) - ValorResidual)/(base.PrazoPagamento - 1)  ;
        }

        public override string ToString()
        {
            return $"\nCrédito Automóvel -> \n{base.ToString()} , \nComissão de abertura: {ComissaoAbertura:C2} euros, \nValor residual (a pagar no último mês): {ValorResidual:C2} euros, \nPrestação mensal: {Prestacoes():C2} euros";
        }
    }
}
