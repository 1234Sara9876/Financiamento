﻿namespace Financiamento
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblValor = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnProcurar = new System.Windows.Forms.Button();
            this.cbFinanciamento = new System.Windows.Forms.ComboBox();
            this.gbCrediHab = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.udRendimento = new System.Windows.Forms.NumericUpDown();
            this.udEuribor = new System.Windows.Forms.NumericUpDown();
            this.lblMontante2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.udPrestacoes2 = new System.Windows.Forms.NumericUpDown();
            this.udSpread = new System.Windows.Forms.NumericUpDown();
            this.udPrazo2 = new System.Windows.Forms.NumericUpDown();
            this.udTaxaJuro2 = new System.Windows.Forms.NumericUpDown();
            this.gbCrediAuto = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lblMontante1 = new System.Windows.Forms.Label();
            this.udPrestacoes1 = new System.Windows.Forms.NumericUpDown();
            this.udValorResidual = new System.Windows.Forms.NumericUpDown();
            this.udComissao1 = new System.Windows.Forms.NumericUpDown();
            this.udPrazo1 = new System.Windows.Forms.NumericUpDown();
            this.udTaxaJuro1 = new System.Windows.Forms.NumericUpDown();
            this.udMontante3 = new System.Windows.Forms.NumericUpDown();
            this.udTaxaJuro3 = new System.Windows.Forms.NumericUpDown();
            this.udPrazo3 = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.udComissao2 = new System.Windows.Forms.NumericUpDown();
            this.udTaxaAmortizacao = new System.Windows.Forms.NumericUpDown();
            this.udPrestacoes3 = new System.Windows.Forms.NumericUpDown();
            this.gbCrediversos = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.gbCrediHab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udRendimento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udEuribor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udSpread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro2)).BeginInit();
            this.gbCrediAuto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udValorResidual)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComissao1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMontante3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComissao2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaAmortizacao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes3)).BeginInit();
            this.gbCrediversos.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(46, 364);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(168, 17);
            this.label6.TabIndex = 64;
            this.label6.Text = "Comissão de Abertura";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(34, 274);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(211, 17);
            this.label5.TabIndex = 59;
            this.label5.Text = "Taxa de Juro Anual Bruta %";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(65, 457);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 17);
            this.label4.TabIndex = 58;
            this.label4.Text = "Valor Residual";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(47, 37);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 17);
            this.label3.TabIndex = 57;
            this.label3.Text = "Preço do Automóvel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 548);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 17);
            this.label2.TabIndex = 56;
            this.label2.Text = "Prestação Mensal";
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(20, 100);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(243, 17);
            this.lblValor.TabIndex = 55;
            this.lblValor.Text = "Prazo de Pagamento(em meses)";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(126, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(208, 23);
            this.label1.TabIndex = 67;
            this.label1.Text = "Tipo de financiamento:";
            // 
            // btnProcurar
            // 
            this.btnProcurar.BackColor = System.Drawing.Color.White;
            this.btnProcurar.Image = ((System.Drawing.Image)(resources.GetObject("btnProcurar.Image")));
            this.btnProcurar.Location = new System.Drawing.Point(872, 28);
            this.btnProcurar.Margin = new System.Windows.Forms.Padding(4);
            this.btnProcurar.Name = "btnProcurar";
            this.btnProcurar.Size = new System.Drawing.Size(56, 44);
            this.btnProcurar.TabIndex = 68;
            this.btnProcurar.UseVisualStyleBackColor = false;
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // cbFinanciamento
            // 
            this.cbFinanciamento.FormattingEnabled = true;
            this.cbFinanciamento.Location = new System.Drawing.Point(397, 38);
            this.cbFinanciamento.Margin = new System.Windows.Forms.Padding(4);
            this.cbFinanciamento.Name = "cbFinanciamento";
            this.cbFinanciamento.Size = new System.Drawing.Size(397, 24);
            this.cbFinanciamento.TabIndex = 69;
            // 
            // gbCrediHab
            // 
            this.gbCrediHab.Controls.Add(this.button1);
            this.gbCrediHab.Controls.Add(this.label23);
            this.gbCrediHab.Controls.Add(this.udRendimento);
            this.gbCrediHab.Controls.Add(this.udEuribor);
            this.gbCrediHab.Controls.Add(this.lblMontante2);
            this.gbCrediHab.Controls.Add(this.label9);
            this.gbCrediHab.Controls.Add(this.label7);
            this.gbCrediHab.Controls.Add(this.label11);
            this.gbCrediHab.Controls.Add(this.label19);
            this.gbCrediHab.Controls.Add(this.label8);
            this.gbCrediHab.Controls.Add(this.label10);
            this.gbCrediHab.Controls.Add(this.udPrestacoes2);
            this.gbCrediHab.Controls.Add(this.udSpread);
            this.gbCrediHab.Controls.Add(this.udPrazo2);
            this.gbCrediHab.Controls.Add(this.udTaxaJuro2);
            this.gbCrediHab.Enabled = false;
            this.gbCrediHab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCrediHab.Location = new System.Drawing.Point(427, 90);
            this.gbCrediHab.Margin = new System.Windows.Forms.Padding(4);
            this.gbCrediHab.Name = "gbCrediHab";
            this.gbCrediHab.Padding = new System.Windows.Forms.Padding(4);
            this.gbCrediHab.Size = new System.Drawing.Size(335, 615);
            this.gbCrediHab.TabIndex = 71;
            this.gbCrediHab.TabStop = false;
            this.gbCrediHab.Text = "Crédito Habitação";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.LightGreen;
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(68, 246);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(198, 45);
            this.button1.TabIndex = 89;
            this.button1.Text = "S I M U L A Ç Ã O";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(65, 177);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(194, 17);
            this.label23.TabIndex = 88;
            this.label23.Text = "Rendimento Mensal Bruto";
            // 
            // udRendimento
            // 
            this.udRendimento.Location = new System.Drawing.Point(87, 197);
            this.udRendimento.Margin = new System.Windows.Forms.Padding(4);
            this.udRendimento.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udRendimento.Name = "udRendimento";
            this.udRendimento.Size = new System.Drawing.Size(160, 23);
            this.udRendimento.TabIndex = 86;
            this.udRendimento.ValueChanged += new System.EventHandler(this.udRendimento_ValueChanged);
            // 
            // udEuribor
            // 
            this.udEuribor.DecimalPlaces = 4;
            this.udEuribor.Enabled = false;
            this.udEuribor.Location = new System.Drawing.Point(93, 487);
            this.udEuribor.Margin = new System.Windows.Forms.Padding(4);
            this.udEuribor.Name = "udEuribor";
            this.udEuribor.Size = new System.Drawing.Size(160, 23);
            this.udEuribor.TabIndex = 85;
            // 
            // lblMontante2
            // 
            this.lblMontante2.BackColor = System.Drawing.SystemColors.Window;
            this.lblMontante2.Location = new System.Drawing.Point(87, 58);
            this.lblMontante2.Name = "lblMontante2";
            this.lblMontante2.Size = new System.Drawing.Size(160, 23);
            this.lblMontante2.TabIndex = 84;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(137, 466);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 83;
            this.label9.Text = "Euribor %";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(104, 548);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(137, 17);
            this.label7.TabIndex = 81;
            this.label7.Text = "Prestação Mensal";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(138, 394);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 17);
            this.label11.TabIndex = 80;
            this.label11.Text = "Spread %";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(73, 322);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(211, 17);
            this.label19.TabIndex = 79;
            this.label19.Text = "Taxa de Juro Anual Bruta %";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(52, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(232, 17);
            this.label8.TabIndex = 78;
            this.label8.Text = "Prazo de Pagamento(em anos)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(104, 32);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 17);
            this.label10.TabIndex = 77;
            this.label10.Text = "Preço da Casa";
            // 
            // udPrestacoes2
            // 
            this.udPrestacoes2.DecimalPlaces = 2;
            this.udPrestacoes2.Enabled = false;
            this.udPrestacoes2.Location = new System.Drawing.Point(91, 568);
            this.udPrestacoes2.Margin = new System.Windows.Forms.Padding(4);
            this.udPrestacoes2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.udPrestacoes2.Name = "udPrestacoes2";
            this.udPrestacoes2.Size = new System.Drawing.Size(160, 23);
            this.udPrestacoes2.TabIndex = 70;
            // 
            // udSpread
            // 
            this.udSpread.DecimalPlaces = 4;
            this.udSpread.Enabled = false;
            this.udSpread.Location = new System.Drawing.Point(93, 413);
            this.udSpread.Margin = new System.Windows.Forms.Padding(4);
            this.udSpread.Name = "udSpread";
            this.udSpread.Size = new System.Drawing.Size(160, 23);
            this.udSpread.TabIndex = 68;
            // 
            // udPrazo2
            // 
            this.udPrazo2.Location = new System.Drawing.Point(87, 123);
            this.udPrazo2.Margin = new System.Windows.Forms.Padding(4);
            this.udPrazo2.Name = "udPrazo2";
            this.udPrazo2.Size = new System.Drawing.Size(160, 23);
            this.udPrazo2.TabIndex = 67;
            this.udPrazo2.ValueChanged += new System.EventHandler(this.udPrazo2_ValueChanged);
            // 
            // udTaxaJuro2
            // 
            this.udTaxaJuro2.DecimalPlaces = 4;
            this.udTaxaJuro2.Enabled = false;
            this.udTaxaJuro2.Location = new System.Drawing.Point(93, 342);
            this.udTaxaJuro2.Margin = new System.Windows.Forms.Padding(4);
            this.udTaxaJuro2.Name = "udTaxaJuro2";
            this.udTaxaJuro2.Size = new System.Drawing.Size(160, 23);
            this.udTaxaJuro2.TabIndex = 66;
            // 
            // gbCrediAuto
            // 
            this.gbCrediAuto.Controls.Add(this.button2);
            this.gbCrediAuto.Controls.Add(this.lblMontante1);
            this.gbCrediAuto.Controls.Add(this.udPrestacoes1);
            this.gbCrediAuto.Controls.Add(this.label2);
            this.gbCrediAuto.Controls.Add(this.lblValor);
            this.gbCrediAuto.Controls.Add(this.udValorResidual);
            this.gbCrediAuto.Controls.Add(this.label4);
            this.gbCrediAuto.Controls.Add(this.udComissao1);
            this.gbCrediAuto.Controls.Add(this.label3);
            this.gbCrediAuto.Controls.Add(this.label6);
            this.gbCrediAuto.Controls.Add(this.label5);
            this.gbCrediAuto.Controls.Add(this.udPrazo1);
            this.gbCrediAuto.Controls.Add(this.udTaxaJuro1);
            this.gbCrediAuto.Enabled = false;
            this.gbCrediAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCrediAuto.Location = new System.Drawing.Point(47, 90);
            this.gbCrediAuto.Margin = new System.Windows.Forms.Padding(4);
            this.gbCrediAuto.Name = "gbCrediAuto";
            this.gbCrediAuto.Padding = new System.Windows.Forms.Padding(4);
            this.gbCrediAuto.Size = new System.Drawing.Size(315, 615);
            this.gbCrediAuto.TabIndex = 72;
            this.gbCrediAuto.TabStop = false;
            this.gbCrediAuto.Text = "Crédito Automóvel";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LightGreen;
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(29, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(198, 45);
            this.button2.TabIndex = 90;
            this.button2.Text = "S I M U L A Ç Ã O";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblMontante1
            // 
            this.lblMontante1.BackColor = System.Drawing.SystemColors.Window;
            this.lblMontante1.Location = new System.Drawing.Point(50, 64);
            this.lblMontante1.Name = "lblMontante1";
            this.lblMontante1.Size = new System.Drawing.Size(158, 23);
            this.lblMontante1.TabIndex = 74;
            // 
            // udPrestacoes1
            // 
            this.udPrestacoes1.DecimalPlaces = 2;
            this.udPrestacoes1.Location = new System.Drawing.Point(48, 567);
            this.udPrestacoes1.Margin = new System.Windows.Forms.Padding(4);
            this.udPrestacoes1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udPrestacoes1.Name = "udPrestacoes1";
            this.udPrestacoes1.Size = new System.Drawing.Size(160, 23);
            this.udPrestacoes1.TabIndex = 5;
            // 
            // udValorResidual
            // 
            this.udValorResidual.DecimalPlaces = 2;
            this.udValorResidual.Enabled = false;
            this.udValorResidual.Location = new System.Drawing.Point(50, 476);
            this.udValorResidual.Margin = new System.Windows.Forms.Padding(4);
            this.udValorResidual.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udValorResidual.Name = "udValorResidual";
            this.udValorResidual.Size = new System.Drawing.Size(160, 23);
            this.udValorResidual.TabIndex = 4;
            // 
            // udComissao1
            // 
            this.udComissao1.DecimalPlaces = 2;
            this.udComissao1.Enabled = false;
            this.udComissao1.Location = new System.Drawing.Point(50, 384);
            this.udComissao1.Margin = new System.Windows.Forms.Padding(4);
            this.udComissao1.Name = "udComissao1";
            this.udComissao1.Size = new System.Drawing.Size(160, 23);
            this.udComissao1.TabIndex = 3;
            // 
            // udPrazo1
            // 
            this.udPrazo1.Location = new System.Drawing.Point(48, 120);
            this.udPrazo1.Margin = new System.Windows.Forms.Padding(4);
            this.udPrazo1.Name = "udPrazo1";
            this.udPrazo1.Size = new System.Drawing.Size(160, 23);
            this.udPrazo1.TabIndex = 2;
            this.udPrazo1.ValueChanged += new System.EventHandler(this.udPrazo1_ValueChanged);
            // 
            // udTaxaJuro1
            // 
            this.udTaxaJuro1.DecimalPlaces = 4;
            this.udTaxaJuro1.Enabled = false;
            this.udTaxaJuro1.Location = new System.Drawing.Point(50, 294);
            this.udTaxaJuro1.Margin = new System.Windows.Forms.Padding(4);
            this.udTaxaJuro1.Name = "udTaxaJuro1";
            this.udTaxaJuro1.Size = new System.Drawing.Size(160, 23);
            this.udTaxaJuro1.TabIndex = 1;
            // 
            // udMontante3
            // 
            this.udMontante3.DecimalPlaces = 2;
            this.udMontante3.Location = new System.Drawing.Point(87, 53);
            this.udMontante3.Margin = new System.Windows.Forms.Padding(4);
            this.udMontante3.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udMontante3.Name = "udMontante3";
            this.udMontante3.Size = new System.Drawing.Size(160, 23);
            this.udMontante3.TabIndex = 65;
            this.udMontante3.ValueChanged += new System.EventHandler(this.udMontante3_ValueChanged);
            // 
            // udTaxaJuro3
            // 
            this.udTaxaJuro3.DecimalPlaces = 4;
            this.udTaxaJuro3.Enabled = false;
            this.udTaxaJuro3.Location = new System.Drawing.Point(92, 294);
            this.udTaxaJuro3.Margin = new System.Windows.Forms.Padding(4);
            this.udTaxaJuro3.Name = "udTaxaJuro3";
            this.udTaxaJuro3.Size = new System.Drawing.Size(160, 23);
            this.udTaxaJuro3.TabIndex = 66;
            // 
            // udPrazo3
            // 
            this.udPrazo3.Location = new System.Drawing.Point(87, 119);
            this.udPrazo3.Margin = new System.Windows.Forms.Padding(4);
            this.udPrazo3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.udPrazo3.Name = "udPrazo3";
            this.udPrazo3.Size = new System.Drawing.Size(160, 23);
            this.udPrazo3.TabIndex = 67;
            this.udPrazo3.ValueChanged += new System.EventHandler(this.udPrazo3_ValueChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(119, 33);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 17);
            this.label16.TabIndex = 73;
            // 
            // udComissao2
            // 
            this.udComissao2.DecimalPlaces = 2;
            this.udComissao2.Enabled = false;
            this.udComissao2.Location = new System.Drawing.Point(90, 383);
            this.udComissao2.Margin = new System.Windows.Forms.Padding(4);
            this.udComissao2.Name = "udComissao2";
            this.udComissao2.Size = new System.Drawing.Size(160, 23);
            this.udComissao2.TabIndex = 68;
            // 
            // udTaxaAmortizacao
            // 
            this.udTaxaAmortizacao.DecimalPlaces = 4;
            this.udTaxaAmortizacao.Enabled = false;
            this.udTaxaAmortizacao.Location = new System.Drawing.Point(90, 476);
            this.udTaxaAmortizacao.Margin = new System.Windows.Forms.Padding(4);
            this.udTaxaAmortizacao.Name = "udTaxaAmortizacao";
            this.udTaxaAmortizacao.Size = new System.Drawing.Size(160, 23);
            this.udTaxaAmortizacao.TabIndex = 69;
            // 
            // udPrestacoes3
            // 
            this.udPrestacoes3.DecimalPlaces = 2;
            this.udPrestacoes3.Enabled = false;
            this.udPrestacoes3.Location = new System.Drawing.Point(84, 567);
            this.udPrestacoes3.Margin = new System.Windows.Forms.Padding(4);
            this.udPrestacoes3.Maximum = new decimal(new int[] {
            2500,
            0,
            0,
            0});
            this.udPrestacoes3.Name = "udPrestacoes3";
            this.udPrestacoes3.Size = new System.Drawing.Size(160, 23);
            this.udPrestacoes3.TabIndex = 70;
            // 
            // gbCrediversos
            // 
            this.gbCrediversos.Controls.Add(this.button3);
            this.gbCrediversos.Controls.Add(this.label13);
            this.gbCrediversos.Controls.Add(this.label15);
            this.gbCrediversos.Controls.Add(this.label17);
            this.gbCrediversos.Controls.Add(this.label20);
            this.gbCrediversos.Controls.Add(this.label14);
            this.gbCrediversos.Controls.Add(this.label12);
            this.gbCrediversos.Controls.Add(this.udPrestacoes3);
            this.gbCrediversos.Controls.Add(this.udTaxaAmortizacao);
            this.gbCrediversos.Controls.Add(this.udComissao2);
            this.gbCrediversos.Controls.Add(this.label16);
            this.gbCrediversos.Controls.Add(this.udPrazo3);
            this.gbCrediversos.Controls.Add(this.udTaxaJuro3);
            this.gbCrediversos.Controls.Add(this.udMontante3);
            this.gbCrediversos.Enabled = false;
            this.gbCrediversos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCrediversos.Location = new System.Drawing.Point(828, 90);
            this.gbCrediversos.Margin = new System.Windows.Forms.Padding(4);
            this.gbCrediversos.Name = "gbCrediversos";
            this.gbCrediversos.Padding = new System.Windows.Forms.Padding(4);
            this.gbCrediversos.Size = new System.Drawing.Size(335, 615);
            this.gbCrediversos.TabIndex = 73;
            this.gbCrediversos.TabStop = false;
            this.gbCrediversos.Text = "Créditos Diversos";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.LightGreen;
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(65, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(198, 45);
            this.button3.TabIndex = 91;
            this.button3.Text = "S I M U L A Ç Ã O";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(100, 548);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(137, 17);
            this.label13.TabIndex = 82;
            this.label13.Text = "Prestação Mensal";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(86, 457);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 17);
            this.label15.TabIndex = 81;
            this.label15.Text = "Taxa de Amortização %";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(86, 364);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(168, 17);
            this.label17.TabIndex = 80;
            this.label17.Text = "Comissão de Abertura";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(70, 274);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(211, 17);
            this.label20.TabIndex = 79;
            this.label20.Text = "Taxa de Juro Anual Bruta %";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(41, 100);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(243, 17);
            this.label14.TabIndex = 78;
            this.label14.Text = "Prazo de Pagamento(em meses)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(119, 33);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(75, 17);
            this.label12.TabIndex = 77;
            this.label12.Text = "Montante";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1220, 733);
            this.Controls.Add(this.gbCrediversos);
            this.Controls.Add(this.gbCrediAuto);
            this.Controls.Add(this.gbCrediHab);
            this.Controls.Add(this.cbFinanciamento);
            this.Controls.Add(this.btnProcurar);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbCrediHab.ResumeLayout(false);
            this.gbCrediHab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udRendimento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udEuribor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udSpread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro2)).EndInit();
            this.gbCrediAuto.ResumeLayout(false);
            this.gbCrediAuto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udValorResidual)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComissao1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udMontante3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaJuro3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrazo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udComissao2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udTaxaAmortizacao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udPrestacoes3)).EndInit();
            this.gbCrediversos.ResumeLayout(false);
            this.gbCrediversos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnProcurar;
        private System.Windows.Forms.ComboBox cbFinanciamento;
        private System.Windows.Forms.GroupBox gbCrediHab;
        private System.Windows.Forms.GroupBox gbCrediAuto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown udPrestacoes2;
        private System.Windows.Forms.NumericUpDown udSpread;
        private System.Windows.Forms.NumericUpDown udPrazo2;
        private System.Windows.Forms.NumericUpDown udTaxaJuro2;
        private System.Windows.Forms.NumericUpDown udPrestacoes1;
        private System.Windows.Forms.NumericUpDown udValorResidual;
        private System.Windows.Forms.NumericUpDown udComissao1;
        private System.Windows.Forms.NumericUpDown udPrazo1;
        private System.Windows.Forms.NumericUpDown udTaxaJuro1;
        private System.Windows.Forms.NumericUpDown udMontante3;
        private System.Windows.Forms.NumericUpDown udTaxaJuro3;
        private System.Windows.Forms.NumericUpDown udPrazo3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown udComissao2;
        private System.Windows.Forms.NumericUpDown udTaxaAmortizacao;
        private System.Windows.Forms.NumericUpDown udPrestacoes3;
        private System.Windows.Forms.GroupBox gbCrediversos;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblMontante2;
        private System.Windows.Forms.NumericUpDown udEuribor;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown udRendimento;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblMontante1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

